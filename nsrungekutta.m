function dy = nsrungekutta(r,y)
dy = zeros(2,1);
dy(1) = (r^2)*(sinh(y(2))-y(2));
dy(2) = (-4*(sinh(y(2))-2*sinh(y(2)/2))*(((r^3)/3)*(sinh(y(2))-8*sinh(y(2)/2)+3*y(2))+y(1)))/((r*(r-2*y(1)))*(cosh(y(2))-4*cosh(y(2)/2)+3));
end