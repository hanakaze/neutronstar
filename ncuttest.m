fn=importdata('names.csv');%file names
efr=zeros(length(fn), 1);
mass=zeros(length(fn), 1);
pn=zeros(length(fn), 1);
S = zeros(length(fn), 1);
t0 = zeros(length(fn), 1);
handler = figure;

Ncut=4;
cutoff=[1.2,1.0,0.5,0.23];

for i=1:length(fn)%number of files
    
            load(char(fn(i)));
            % R=(sinh(Y(:,2))-Y(:,2)-2*((sinh(Y(:,2)/4)).^3)/3)/(4*pi);
			R=(sinh(Y(:,2))-Y(:,2))/(4*pi);
			efr(i)=r_b;
			mass(i)=u_b;
            r=T;
            pn(i)=dot((sinh(Y(:,2)/4)).^3,T.*T.*(sqrt(1/(1-2*Y(:,1)./T)))')*2*T(1)/3.0;
			t0(i)=Y(1,2);
            hr=T(2)-T(1);
		
	for j=1:1:Ncut
            hk=0.001;
			k=linspace(pi/(cutoff(j)*efr(i)),100+pi/(cutoff(j)*efr(i)),1000);
			k=k';
			hk=k(2)-k(1);
			
			K = (k.^(-1)*r').*sin(k*r')*hr;
			fft= K*R;%fourier mode
            
            % fk=abs(fft);
			fk=fft.*fft;    %modal fraction
            
% 			ftot=dot(fk,k.*k)
            
            %fkmax=(hr*dot(r.*r,R))^2;%the most weight at k=0
			fkmax=max(fk);
% 			fkmax=ftot; %normalize to 1
			
			fk=fk/fkmax;%first most weight
%         
 S(i,j)=dot((fk.*log(fk)),(k.*k));
			S(i,j)=-4*pi*hk*S(i,j);
			legends{j,1} = strcat('t0=',num2str(t0(i)),'Cutoff=', num2str(cutoff(j)));
% 			save(strcat('t0=',num2str(t0(i)),'Cutoff=', num2str(cutoff(j)),'.mat'),'k','fft','fk','r','R');
    end
    
end
save('results.mat','t0','cutoff','S','efr','mass','t0','pn');
plot(t0,S),xlabel('t_0'),ylabel('Entropy'), legend(legends);
saveas(handler,'phiS.fig');
quit;