alpha= -1;
for i=1:1:length(t0)
    SS(i,:)=4*pi*S(i,:).*((sinh(t0(i))-t0(i))^alpha)*mass(i);
    t0(i) = (sinh(t0(i))-t0(i))/(4*pi);
%     S(i,:)=smooth(S(i,:));
end

NEntropy=SS;
hold on;
N=length(cutoff);
cc=hsv(N);
legends=cell(N,1);
startIn=1; %starting index 
Ncut =1; %number of cut observed
figure;
ylim([0,500]);
xlim([0,40]);
for i=1:1:Ncut
    hold on;
    h(i)=plot(t0,NEntropy(:,i+startIn),'Marker','o','color',cc(i,:),'LineWidth',1);
    legends{i,1}=strcat('\pi/(',num2str(cutoff(i+startIn),'%.3f'),'R)');
    [val,ind]=findpeaks(-NEntropy(:,i+startIn));
    plot(t0(ind),-val,'o','MarkerSize',6,'MarkerFaceColor','auto');
     [val,ind]=findpeaks(NEntropy(:,i+startIn));
    plot(t0(ind),val,'d','MarkerSize',6,'MarkerFaceColor','auto');
end
axes('Position',[0.5,0.5,.2,.2]);
[val,ind]= max(mass);
scatter(t0(ind),NEntropy(ind));
hleg=legend(h(1:Ncut),legends{1:Ncut,1});
xlabel('t_0','FontSize',16);
ylabel('S\rho_0^{-1}','FontSize',16,'Rotation',0);
title('S\rho_0^{-1} vs. t_0','FontSize',20)
% htitle = get(hleg,'Title');
% set(htitle,'String','\pi/k_{min}R');
