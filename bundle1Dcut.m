NEntropy=S;
gamma=t0;
hold on;
N=1;
cc=hsv(N);
legends=cell(N,1);
startIn=0;
for i=1:1:N
% 
%     plot(Rho0(i,:),NEntropy(i,:));
%     [val,ind]=findpeaks(-NEntropy(i,:));
%     plot(Rho0(i,ind),-val,'o');
%      [val,ind]=findpeaks(NEntropy(i,:));
%     plot(Rho0(i,ind),val,'X');
%     
%   plot(Gamma(:,i),NEntropy(:,i));
%     [val,ind]=findpeaks(-NEntropy(:,i));
%     plot(Gamma(ind,i),-val,'o');
%      [val,ind]=findpeaks(NEntropy(:,i));
%     plot(Gamma(ind,i),val,'X');
% 
%    NEntropy(:,i+startIn)=smooth(NEntropy(:,i+startIn));
    h(i)=plot(gamma,NEntropy(:,i+startIn),'color',cc(i,:),'LineWidth',2);
    legends{i,1}=strcat('\pi/(',num2str(cutoff(i+startIn),'%.3f'),'R)');
    [val,ind]=findpeaks(-NEntropy(:,i+startIn));
    plot(gamma(ind),-val,'o','MarkerSize',6,'MarkerFaceColor','auto');
     [val,ind]=findpeaks(NEntropy(:,i+startIn));
    plot(gamma(ind),val,'d','MarkerSize',6,'MarkerFaceColor','auto');

end
hleg=legend(h,legends);
line([3.04,3.04],[0,10000],'LineWidth',2,'LineStyle','-.');
% line([5/3,5/3],[4,6],'LineWidth',2,'LineStyle','-.');
xlabel('t_0','FontSize',16);
ylabel('S','FontSize',16,'Rotation',0);
title('S vs. t_0 with Various Cutoffs','FontSize',20)
% htitle = get(hleg,'Title');
% set(htitle,'String','\pi/k_{min}R');
