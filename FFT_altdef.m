fn=importdata('names.csv');%file names
efr=zeros(length(fn), 1);
mass=zeros(length(fn), 1);
pn=zeros(length(fn), 1);
S = zeros(length(fn), 1);
t0 = zeros(length(fn), 1);
handler = figure;
Cutoff=1

for i=1:length(fn)%number of files
    
            load(char(fn(i)));
            % R=(sinh(Y(:,2))-Y(:,2)-2*((sinh(Y(:,2)/4)).^3)/3)/(4*pi);
			R=(sinh(Y(:,2))-Y(:,2))/(4*pi);
			efr(i)=r_b;
			mass(i)=u_b;
            r=T;
            pn(i)=dot((sinh(Y(:,2)/4)).^3,T.*T.*(sqrt(1/(1-2*Y(:,1)./T)))')*2*T(1)/3.0;
			t0(i)=Y(1,2);
            hr=T(2)-T(1);
		
	for cutoff=1:1:1
            hk=0.001;
			k=linspace(hk,20,10000);
			k=k';
			hk=k(2)-k(1);
			
			K = (k.^(-1)*r').*sin(k*r')*hr;
			fft= K*R;%fourier mode
			fk=fft.*fft;%modal fraction
			
			ftot=4*pi*dot(fk,k.*k)*hk;
%             fk=fk*((pi/efr(i))^3)/ftot;
            fk=4*pi*fk.*k.*k*hk/ftot;
            
            
%             %fkmax=(hr*dot(r.*r,R))^2;%the most weight at k=0
% 			fkmax=max(fk)
% % 			fkmax=ftot; %normalize to 1
% 			
% 			fk=fk/fkmax;%first most weight
						
			S(i,cutoff)=dot((fk.*log(fk)),(k.*k));
			S(i,cutoff)=-4*pi*hk*S(i,cutoff);
			%legends{cutoff,1} = strcat('Cutoff=', num2str((0.95+0.01*(cutoff-1))));
			legends{cutoff,1} = strcat('t0=',num2str(t0(i)),'Cutoff=', num2str(cutoff));
            %if (S(i)/St-1 <1E-3)
             %    break;
            %end
            %St = S(i);
			%plot(k,fk);
% 			save(strcat('t0=',num2str(t0(i)),'Cutoff=', num2str(cutoff),'.mat'),'k','fft','fk','r','R');
    end
    
end
save('results_altdef.mat','t0','cutoff','S','efr','mass','t0','pn');
plot(t0,S),xlabel('t_0'),ylabel('Entropy'), legend(legends);
saveas(handler,'phiS_altdef.fig');
quit;