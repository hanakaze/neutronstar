a = 0.0;
b = 0.02;
N = 200000;
h = (b-a)/N;
options = odeset('MaxStep',h);
r = linspace(h, b, N);
t_b=0.0;
r_b=0.0;
u_b=0.0;
        t_i(i)=50;
        [T Y] = ode45(@nsrungekutta,r,[0 t_i(i)],options);
        ind=find(Y(:,2) > 0);
        T=T(1:ind(end));
        Y=Y(1:ind(end),:);
		r_b=T(ind(end));
		u_b=Y(ind(end),1);
		t_b=Y(ind(end),2);
save(strcat(num2str(t_i(i),'%0.3f'),'solution.mat'),'T','Y','r_b','t_b','u_b');

